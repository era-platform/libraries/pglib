%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.05.2021
%%% @doc pglib application (connector and connection pool).
%%%   Application could be setup by application:set_env:
%%%      log_destination
%%%          descriptor to define log file folder and prefix.
%%%          Default: {'env','reg'}
%%%      pool_max_connections
%%%          How many connections to same connection string should be used.
%%%          Default: 10
%%%      pool_connection_ttl
%%%          How long to keep unused connection, in seconds
%%%          Default: 60
%%%      reconnect_attempts_count
%%%          How many times connection should retry after first failure (if several connection strings, then after first cycle)
%%%          Default: 2 (total attempts: 3)
%%%      reconnect_pause_ms
%%%          How long to sleep before retry connect, in milliseconds
%%%          Default: 1000

-module(pglib).
-author('Peter <tbotc@yandex.ru>').

-behaviour(application).

% client facade
-export([get_connection/2,
         release_connection/2]).
-export([connect_db/1,
         connect_db_returnargs/1,
         close_db/1,
         query_db/2,
         query_db_tout/3,
         try_query_to_db/2, try_query_to_db/3,
         ext_query_db/3]).

% application
-export([start/0,
         stop/0]).

% application
-export([start/2,
         stop/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public
%% ====================================================================

%% -----------------------------------------------------
%% LOADER FACADE
%% -----------------------------------------------------

%% -------------------------------------
%% Starts application
%% -------------------------------------
start() ->
    case application:ensure_all_started(?APP, permanent) of
        {ok, _Started} -> ok;
        Error -> Error
    end.

%% -------------------------------------
%% Stops application
%% -------------------------------------
stop() -> application:stop(?APP).

%% ---------------------------------------------------------------
%% POOL Functions
%% ---------------------------------------------------------------

%% ----------------------------------------------
%% Request and return connection from pool
%%    Hides async procedure.
%% ----------------------------------------------
-spec get_connection(DbParams::map() | [map()] | [{Key::atom(),Value::string()}] | [[{Key::atom(),Value::string()}]],
                     Timeout::non_neg_integer()) -> {ok,Conn::pid()} | {error,Reason::term()}.
%%----------------------------------------------
get_connection(DbParams,Timeout) ->
    ?POOLSRV:get_connection(DbParams,Timeout).

%% ----------------------------------------------
%% return used connection to pool
%% ----------------------------------------------
-spec release_connection(DbParams::map() | [map()] | [{Key::atom(),Value::string()}] | [[{Key::atom(),Value::string()}]],
                         Conn::pid()) -> ok.
%% ----------------------------------------------
release_connection(DbParams,Conn) ->
    ?POOLSRV:release_connection(DbParams,Conn).

%% ---------------------------------------------------------------
%% Connection functions
%% ---------------------------------------------------------------

%% ----------------------------------------------
%% Connects and return connection process.
%%   Opts: host, port, login, pwd, database,
%%   Optional: ignore_recovery (default: true), skip_monitor (default: false)
%% ----------------------------------------------
-spec connect_db(Params :: map() | [map()] | PropList | [PropList]) ->
                    {ok,Conn::pid()} | {error, Reason::term()} | any()
    when PropList::[{K::atom(),V::binary()|integer()}].
%% ----------------------------------------------
connect_db(Params) -> ?Connector:connect_db(Params).

%% ----------------------------------------------
%% Attempt connect to db (one of connection props) and return used connection props on success
%% ----------------------------------------------
-spec connect_db_returnargs(Params :: map() | [map()] | PropList | [PropList]) ->
                                {ok,Conn::pid(),Args::map()} | {error, Reason::term()} | any()
    when PropList::[{K::atom(),V::binary()|integer()}].
%%----------------------------------------------
connect_db_returnargs(Params) -> ?Connector:connect_db_returnargs(Params).

%% ----------------------------------------------
%% Close connection process
%% ----------------------------------------------
-spec close_db(Conn::pid()) -> ok.
%% ----------------------------------------------
close_db(Conn) -> ?Connector:close_db(Conn).

%% ----------------------------------------------
%% Simple query to postgresql
%% ----------------------------------------------
-spec query_db(Conn::pid(), Sql::string()) -> Result::term().
%% ----------------------------------------------
query_db(Conn, Sql) -> ?Connector:query_db(Conn, Sql).

%% ----------------------------------------------
%% Query to postgresql with timeout
%% ----------------------------------------------
-spec query_db_tout(Conn::pid(), Sql::string(), Timeout::integer()|infinity) -> Result::term().
%% ----------------------------------------------
query_db_tout(Conn, Sql, Timeout) -> ?Connector:query_db_tout(Conn, Sql, Timeout).

%% ----------------------------------------------
%% epgsql Extended Query with params
%% ----------------------------------------------
-spec ext_query_db(Conn::pid(), Sql::string(), Params::[term()]) -> Result::term().
%% ----------------------------------------------
ext_query_db(Conn, Sql, Params) -> ?Connector:ext_query_db(Conn, Sql, Params).

%% ----------------------------------------------
%% query to db in try..catch
%% ----------------------------------------------
-spec try_query_to_db(Conn::pid(),Sql::string()) ->
    {ok,Res::term()} | {error,Reason::term()} | [{ok,Res::term()}|{ok,Headings::term(),Res::term()}].
%%----------------------------------------------
try_query_to_db(Conn,Sql) -> ?Connector:try_query_to_db(Conn,Sql).

%% ----------------------------------------------
%% query to db in try..catch with timeout
%% ----------------------------------------------
-spec try_query_to_db(Conn::pid(),Sql::string(),Timeout::integer()|infinity) ->
    {ok,Res::term()} | {error,Reason::term()} | [{ok,Res::term()}|{ok,Headings::term(),Res::term()}].
%% ----------------------------------------------
try_query_to_db(Conn,Sql,Timeout) -> ?Connector:try_query_to_db(Conn,Sql,Timeout).

%% ===================================================================
%% Application callback functions
%% ===================================================================

%% -------------------------------------
%% @doc Starts application
%% -------------------------------------
start(_, _) ->
    setup_start_ts(),
    setup_dependencies(),
    ensure_deps_started(),
    ?SUPV:start_link(undefined).

%% -------------------------------------
%% @doc Stops application.
%% -------------------------------------
stop(_State) ->
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% --------------------------------------
%% @private
%% setup start_utc to applicaiton's env for monitor purposes
%% --------------------------------------
setup_start_ts() ->
    application:set_env(?APP,start_utc,?BU:timestamp()).

%% --------------------------------------
%% @private
%% setup opts of dependency apps
%% --------------------------------------
setup_dependencies() -> ok.

%% --------------------------------------
%% @private
%% starts deps applications, that are not linked to app, and to ensure
%% --------------------------------------
ensure_deps_started() ->
    {ok,_} = application:ensure_all_started(?BASICLIB, permanent),
    {ok,_} = application:ensure_all_started(epgsql, permanent),
    ok.