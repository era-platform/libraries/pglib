%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.05.2021
%%% @doc Application options access

-module(pglib_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    log_destination/1,
    pool_max_connections/0,
    pool_connection_ttl/0,
    reconnect_attempts_count/1,
    reconnect_pause_ms/1
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(POOL_MAX_CONNECTIONS, 10).
-define(POOL_CONNECTION_TTL, 60).

%% ====================================================================
%% API functions
%% ====================================================================

%%
log_destination(Default) ->
    ?BU:get_env(?APP, 'log_destination', Default).

%%
pool_max_connections() ->
    ?BU:get_env(?APP, 'pool_max_connections', ?POOL_MAX_CONNECTIONS).

%%
pool_connection_ttl() ->
    ?BU:get_env(?APP, 'pool_connection_ttl', ?POOL_CONNECTION_TTL).

%%
reconnect_attempts_count(Default) ->
    ?BU:get_env(?APP, 'reconnect_attempts_count', Default).

%%
reconnect_pause_ms(Default) ->
    ?BU:get_env(?APP, 'reconnect_pause_ms', Default).

%% ====================================================================
%% Internal functions
%% ====================================================================


