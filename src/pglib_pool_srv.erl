%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.05.2022
%%% @doc Connetion pool

-module(pglib_pool_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([get_connection/2,
         release_connection/2]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-define(StoreTimeout, ?CFG:pool_connection_ttl() * 1000).

-define(MaxConnections, ?CFG:pool_max_connections()).

-record(poolstate, {
    free_conns = #{}, % map of free connection params by DbParams
    used_conns = [] :: [{DbParams::map()|tuple(),Conn::term(),Pid::pid(),MonRef::reference(),ConnInfo::map()}], % list of busy connection params
    waitors = #{}, % list of waitor processes by DbParams #{DbParams => [{FromPid,MonRef,Ref}]}
    syncers = [] :: {FromPid::pid(),MonRef::reference(),Ref::reference(),TimerRef::reference(),DbParams::map(),Conn::term(),ConnItem::map()}, % list of waitor on sync (wait thankyou)
    connectors = [] :: [{Ref::reference(),AsyncPid::pid(),MonRef::reference(),DbParams::map()|tuple()}], % list of creating connections
    conns = #{} :: map(), % map of conn -> DbParams
    refmap = #{}, % map of request-ref -> DbParams key
    ref :: reference(),
    timerref :: reference()
}).

%% ====================================================================
%% API functions
%% ====================================================================

start_link() ->
    Opts = [],
    gen_server:start_link({local,?MODULE}, ?MODULE, Opts, []).

%% ====================================================================
%% Get connection routines
%% ====================================================================

%% -----------------------------------
%% Request and return connection from pool
%%    Hides async procedure.
%% -----------------------------------
%% -> request
%%    <- {conn,Conn}
%% <- {ok,Conn
%% -> request
%%    <- {ok,Ref}
%%    <- {conn,Ref,Conn}
%%    -> {thankyou,Ref}
%% <- {ok,Conn}
%%
%% -> request
%% <- {ok,Ref}
%% -> {cancel,Ref}
%% -----------------------------------
-spec get_connection(DbParams, Timeout::non_neg_integer())
      -> {ok,Conn::pid()} | {error,Reason::term()}
    when DbParams :: map()
                   | [map()]
                   | [{Key::atom(),Value::string()}]
                   | [[{Key::atom(),Value::string()}]]
                   | {Key::term(), PoolSize::integer(), [map()]}.
%% -----------------------------------
get_connection(DbParams,Timeout) ->
    PoolPid = whereis(?MODULE),
    case gen_server:call(PoolPid,{get_connection,DbParams,self()},erlang:min(Timeout,5000)) of
        {error,_}=Err -> Err;
        {conn,Conn} ->
            {ok,Conn};
        {ok,Ref} ->
            MonRef = erlang:monitor(process,PoolPid),
            receive
                {conn,Ref,Conn} ->
                    PoolPid ! {thankyou,Ref},
                    erlang:demonitor(MonRef),
                    {ok,Conn};
                {failed,Ref,Error} ->
                    erlang:demonitor(MonRef),
                    Error;
                {'DOWN',MonRef,process,PoolPid,_Reason} ->
                    {error,{internal_error,<<"pg connection pool process failed">>}}
            after Timeout ->
                PoolPid ! {cancel,Ref},
                {error,timeout}
            end
    end.

%% -----------------------------------
%% return used connection to pool
%% -----------------------------------
-spec release_connection(DbParams, Conn::pid()) -> ok
    when DbParams :: map()
                   | [map()]
                   | [{Key::atom(),Value::string()}]
                   | [[{Key::atom(),Value::string()}]]
                   | {PoolKey::term(), PoolSize::integer(), [map()]}.
%% -----------------------------------
release_connection(DbParams,Conn) ->
    gen_server:cast(?MODULE,{release_connection,DbParams,Conn}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(_Opts) ->
    % ----
    Ref = make_ref(),
    State = #poolstate{ref=Ref,
                       timerref= erlang:send_after(?StoreTimeout, self(), {check,Ref})},
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% --------------
%% returns current node
handle_call({get_node}, _From, State) ->
    {reply, {node(), self()}, State};

%% --------------
%% print state
handle_call({print}, _From, #poolstate{}=State) ->
    ?LOG('$force', "PG connection pool state: ~n\t~120tp", [State]),
    {reply, ok, State};

%% --------------
%% restart gen_serv
handle_call({restart}, _From, State) ->
    {stop, restart, ok, State};

%% --------------
%% get connection from pool
handle_call({get_connection,DbParams,FromPid}, _From, State) ->
    get_connection(DbParams,FromPid,State);

%% --------------
%% other
handle_call(_Msg, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% --------------
%% release connection after usage
handle_cast({release_connection,DbParams,Conn}, State) ->
    State1 = release_connection(DbParams,Conn,State),
    {noreply,State1};

%% --------------
%% other
handle_cast(_Msg, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
%% Timer of cache filtering
%% --------------
handle_info({check,Ref}, #poolstate{ref=Ref}=State) ->
    State1 = check_connections(State),
    Ref1 = make_ref(),
    State2 = State1#poolstate{ref = Ref1,
                              timerref = erlang:send_after(?StoreTimeout, self(), {check,Ref1})},
    {noreply, State2};

%% --------------
%% Query Process DOWN
%% --------------
handle_info({'DOWN',MonRef,process,Pid,Reason}, #poolstate{}=State) ->
    State1 = on_process_down(MonRef,Pid,Reason,State),
    {noreply, State1};

%% --------------
%% Connection process DOWN
%% --------------
handle_info({'EXIT',Conn,{_Reason}},#poolstate{}=State) ->
    State1 = drop_connection(Conn,State),
    {noreply, State1};

%% --------------
%% cancel waiting for connection
%% --------------
handle_info({cancel,Ref}, State) ->
    State1 = cancel_waiting(Ref,State),
    {noreply,State1};

%% --------------
%% acknowledge for connection
%% --------------
handle_info({thankyou,Ref}, State) ->
    State1 = acknowledge_result(Ref,State),
    {noreply,State1};

%% --------------
%% when syncer timeout
%% --------------
handle_info({syncer_timeout,Ref}, State) ->
    State1 = syncer_timeout(Ref,State),
    {noreply,State1};

%% --------------
handle_info({conn_created,Ref,Conn},State) ->
    State1 = conn_created(Ref,Conn,State),
    {noreply,State1};

%% --------------
handle_info({conn_failed,Ref,Reason},State) ->
    State1 = conn_failed(Ref,Reason,State),
    {noreply,State1};

%% --------------
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, #poolstate{}=_State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------------------------
check_connections(#poolstate{free_conns=FreeConns,conns=Conns}=State) ->
    NowTS = ?BU:timestamp(),
    F = fun(DbParams,ConnInfos,{AccToClose,AccRest}) ->
            Fpart = fun(ConnInfo) ->
                        Conn = maps:get(conn,ConnInfo),
                        case process_info(Conn,status) of
                            undefined -> true;
                            _ ->
                                StoreTimeout = ?StoreTimeout,
                                case maps:get(free_ts,ConnInfo) of
                                    FreeTS when NowTS - FreeTS > StoreTimeout -> true;
                                    _ -> false
                                end end end,
            {ToClose,RestC} = lists:partition(Fpart, ConnInfos),
            {[maps:get(conn,ConnInfo) || ConnInfo <- ToClose] ++ AccToClose, update_map_list(AccRest,DbParams,RestC)}
        end,
    {ConnsToClose,FreeConns1} = maps:fold(F, {[],FreeConns}, FreeConns),
    Self = self(),
    spawn(fun() -> lists:foreach(fun(Conn) ->
                                        ?BLmonitor:drop_fun(Self, Conn),
                                        ?BLmonitor:drop_fun(Conn, Self),
                                        catch ?Connector:close_db(Conn)
                                 end, ConnsToClose) end),
    State#poolstate{free_conns=FreeConns1,
                    conns=maps:without(ConnsToClose,Conns)}.

%% -----------------------------
-spec get_connection(DbParams::map(),FromPid::pid(),State::#poolstate{}) ->
    {reply,{conn,Conn::term()},#poolstate{}} | {noreply,#poolstate{}}.
%% -----------------------------
get_connection(DbParams,FromPid,#poolstate{free_conns=FreeConns,used_conns=UsedConns}=State) ->
    case maps:get(DbParams,FreeConns,[]) of
        [ConnItem|RestC] ->
            Conn = maps:get(conn,ConnItem),
            MonRef = erlang:monitor(process,FromPid),
            State1 = State#poolstate{free_conns=update_map_list(FreeConns,DbParams,RestC),
                                     used_conns=[{Conn,DbParams,FromPid,MonRef,used(ConnItem)}|UsedConns]},
            {reply,{conn,Conn},State1};
        [] ->
            RequestRef = make_ref(),
            MonRef = erlang:monitor(process,FromPid),
            #poolstate{waitors=Waitors,
                       refmap=RefMap}=State,
            WaitorsList = maps:get(DbParams,Waitors,[]),
            Waitor = {FromPid,MonRef,RequestRef},
            State1 = State#poolstate{waitors=update_map_list(Waitors,DbParams,WaitorsList ++ [Waitor]),
                                     refmap=RefMap#{RequestRef => DbParams}},
            State2 = add_connection(DbParams,State1),
            {reply,{ok,RequestRef},State2}
    end.

%% -----------------------------
-spec release_connection(DbParams::map(),Conn::term(),State::#poolstate{}) -> #poolstate{}.
%% -----------------------------
release_connection(DbParams,Conn,#poolstate{free_conns=FreeConns,used_conns=UsedConns,waitors=Waitors,syncers=Syncers}=State) ->
    case lists:keytake(Conn,1,UsedConns) of
        false -> State;
        {value,{Conn,DbParams,_FromPid,MonRef0,ConnItem},RestC} ->
            erlang:demonitor(MonRef0),
            FreeList = maps:get(DbParams,FreeConns,[]),
            case maps:get(DbParams,Waitors,[]) of
                [] ->
                    State#poolstate{free_conns=update_map_list(FreeConns,DbParams,[free(ConnItem)|FreeList]),
                                    used_conns=RestC};
                [{FromPid,MonRef1,RequestRef}|RestW] ->
                    FromPid ! {conn,RequestRef,Conn},
                    TimerRef = erlang:send_after(5000,self(),{syncer_timeout,RequestRef}),
                    Syncer = {FromPid,MonRef1,RequestRef,TimerRef,DbParams,Conn,ConnItem},
                    State#poolstate{waitors=update_map_list(Waitors,DbParams,RestW),
                                    syncers=[Syncer|Syncers],
                                    used_conns=[{Conn,DbParams,FromPid,MonRef1,used(free(ConnItem))}|RestC]}
            end;
        _ ->
            ?LOG('$warning', "PG connection pool. Invalid connection released. Skipping..."),
            State
    end.

%% -----------------------------
-spec cancel_waiting(RequestRef::reference(),State::#poolstate{}) -> #poolstate{}.
%% -----------------------------
cancel_waiting(RequestRef,#poolstate{waitors=Waitors,refmap=RefMap}=State) ->
    case maps:get(RequestRef,RefMap,undefined) of
        undefined ->
            ?LOG('$warning', "PG connection pool. Invalid cancel received. Skipping..."),
            State;
        DbParams ->
            WaitorsList = maps:get(DbParams,Waitors,[]),
            case lists:keytake(RequestRef,3,WaitorsList) of
                false ->
                    ?LOG('$warning', "PG connection pool. Invalid cancel received. Reference conflict. Skipping..."),
                    State;
                {value,{_FromPid,MonRef,RequestRef},RestW} ->
                    erlang:demonitor(MonRef),
                    State#poolstate{waitors=update_map_list(Waitors,DbParams,RestW),
                                    refmap=maps:without([RequestRef],RefMap)}
            end
    end.

%% -----------------------------
-spec acknowledge_result(RequestRef::reference(),State::#poolstate{}) -> #poolstate{}.
%% -----------------------------
acknowledge_result(RequestRef,#poolstate{syncers=Syncers,refmap=RefMap}=State) ->
    case lists:keytake(RequestRef,3,Syncers) of
        false ->
            ?LOG('$warning', "Postgres connection pool. Invalid acknowledge received. Skipping..."),
            State;
        {value,{_FromPid,_MonRef,RequestRef,TimerRef,_DbParams,_Conn,_ConnItem},RestS} ->
            ?BU:cancel_timer(TimerRef),
            State#poolstate{syncers=RestS,
                            refmap=maps:without([RequestRef],RefMap)}
    end.

%% -----------------------------
-spec syncer_timeout(RequestRef::reference(),State::#poolstate{}) -> #poolstate{}.
%% -----------------------------
syncer_timeout(RequestRef,#poolstate{syncers=Syncers,refmap=RefMap}=State) ->
    case lists:keytake(RequestRef,3,Syncers) of
        false ->
            ?LOG('$warning', "PG connection pool. Invalid syncer_timeout received. Skipping..."),
            State;
        {value,{_FromPid,MonRef,RequestRef,_TimerRef,DbParams,Conn,_ConnItem},RestS} ->
            erlang:demonitor(MonRef),
            State1 = State#poolstate{syncers=RestS,
                                     refmap=maps:without([RequestRef],RefMap)},
            % TODO may be need to close connection
            release_connection(DbParams,Conn,State1)
    end.

%% -----------------------------
-spec on_process_down(MonRef::reference(),Pid::pid(),Reason::term(),State::#poolstate{}) -> #poolstate{}.
%% -----------------------------
on_process_down(MonRef,Pid,_Reason,#poolstate{used_conns=UsedConns,waitors=Waitors,syncers=Syncers,connectors=Connectors,refmap=RefMap}=State) ->
    % check in used
    case lists:keyfind(MonRef,4,UsedConns) of
        {Conn,DbParams,Pid,MonRef,_ConnItem} ->
            % TODO may be need to close connection
            release_connection(DbParams,Conn,State);
        false ->
            % check in syncers
            case lists:keytake(MonRef,2,Syncers) of
                {value,{_FromPid,MonRef,RequestRef,TimerRef,DbParams,Conn,_ConnItem},RestS} ->
                    ?BU:cancel_timer(TimerRef),
                    State1 = State#poolstate{syncers=RestS,
                                             refmap=maps:without([RequestRef],RefMap)},
                    % TODO may be need to close connection
                    release_connection(DbParams,Conn,State1);
                false ->
                    % check in waitors
                    case maps:get(MonRef,RefMap,undefined) of
                        DbParams when DbParams/=undefined ->
                            WaitorsList = maps:get(DbParams,Waitors,[]),
                            case lists:keytake(MonRef,2,WaitorsList) of
                                false ->
                                    ?LOG('$warning', "PG connection pool. Invalid DOWN received. Reference conflict. Skipping..."),
                                    State;
                                {value,{_FromPid,MonRef,RequestRef},RestW} ->
                                    State#poolstate{waitors=update_map_list(Waitors,DbParams,RestW),
                                                    refmap=maps:without([RequestRef],RefMap)}
                            end;
                        undefined ->
                            case lists:keyfind(MonRef,3,Connectors) of
                                false ->
                                    ?LOG('$trace', "PG connection pool. Invalid DOWN received (from ~120p). Skipping...",[Pid]),
                                    State;
                                {Ref,_,_,_} ->
                                    Error = {error,{internal_error,<<"Connection process failure">>}},
                                    conn_failed(Ref,Error,#poolstate{waitors=Waitors,refmap=RefMap,connectors=Connectors}=State)
                            end
                    end
            end
    end.

%% -----------------------------
-spec conn_created(Ref::reference(),Conn::term(),State::#poolstate{}) -> #poolstate{}.
%% -----------------------------
conn_created(Ref,Conn,#poolstate{free_conns=FreeConns,used_conns=UsedConns,
                                 waitors=Waitors,syncers=Syncers,
                                 connectors=Connectors,conns=Conns}=State) ->
    case lists:keytake(Ref,1,Connectors) of
        false ->
            ?LOG('$warning', "PG connection pool. Invalid conn_created received. Reference conflict. Skipping..."),
            spawn(fun() -> catch ?Connector:close_db(Conn) end),
            State;
        {value,{Ref,_AsyncPid,MonRefA,DbParams},RestC} ->
            erlang:demonitor(MonRefA),
            % monitor instead
            Self = self(),
            ?BLmonitor:append_fun(Self, Conn, fun(_) -> ?LOG('$warning', "Monitor. Auto closing db connection ~p owned by ~p", [Conn,Self]), ?Connector:close_db(Conn) end),
            ?BLmonitor:append_fun(Conn, Self, fun(Reason) -> Self ! {'EXIT',Conn,{Reason}} end),
            %
            State1 = State#poolstate{connectors=RestC,
                                     conns=Conns#{Conn => DbParams}},
            ConnItem = new_conn(DbParams,Conn),
            case maps:get(DbParams,Waitors,[]) of
                [] ->
                    FreeList = maps:get(DbParams,FreeConns,[]),
                    State1#poolstate{free_conns=update_map_list(FreeConns,DbParams,[ConnItem|FreeList])};
                [{FromPid,MonRef,RequestRef}|RestW] ->
                    FromPid ! {conn,RequestRef,Conn},
                    TimerRef = erlang:send_after(5000,self(),{syncer_timeout,RequestRef}),
                    Syncer = {FromPid,MonRef,RequestRef,TimerRef,DbParams,Conn,ConnItem},
                    State1#poolstate{waitors=update_map_list(Waitors,DbParams,RestW),
                                     syncers=[Syncer|Syncers],
                                     used_conns=[{Conn,DbParams,FromPid,MonRef,used(ConnItem)}|UsedConns]}
            end end.

%% -----------------------------
-spec conn_failed(Ref::reference(),Reason::term(),State::#poolstate{}) -> #poolstate{}.
%% -----------------------------
conn_failed(Ref,Error,#poolstate{waitors=Waitors,refmap=RefMap,connectors=Connectors}=State) ->
    case lists:keytake(Ref,1,Connectors) of
        false ->
            ?LOG('$warning', "PG connection pool. Invalid conn_failed received. Reference conflict. Skipping..."),
            State;
        {value,{Ref,_AsyncPid,MonRefA,DbParams},RestC} ->
            erlang:demonitor(MonRefA),
            State1 = State#poolstate{connectors=RestC},
            case maps:get(DbParams,Waitors,[]) of
                [] -> State1;
                [{FromPid,MonRef,RequestRef}|RestW] ->
                    FromPid ! {failed,RequestRef,Error},
                    erlang:demonitor(MonRef),
                    State1#poolstate{waitors=update_map_list(Waitors,DbParams,RestW),
                                     refmap=maps:without([RequestRef],RefMap)}
            end end.

%% ---------------------------------------------
%% @private
add_connection(DbParams,#poolstate{used_conns=UsedConns,connectors=Connectors}=State) ->
    UsedList = lists:filter(fun({_,DbParamsX,_,_,_}) -> DbParamsX==DbParams end, UsedConns),
    InitingList = lists:filter(fun({_,_,_,DbParamsX}) -> DbParamsX==DbParams end, Connectors),
    MaxConnections = max_connections(DbParams),
    case length(UsedList)+length(InitingList) of
        N when N < MaxConnections -> init_new_conn(DbParams,State);
        _ -> State
    end.

%% @private
max_connections({_,PoolSize,_}) -> PoolSize;
max_connections(_) -> ?MaxConnections.

%% @private
connection_params({_,_,ConnParams}) -> ConnParams;
connection_params(DbParams) -> DbParams.

%% @private
init_new_conn(DbParams,#poolstate{connectors=Connectors}=State) ->
    {Self,Ref} = {self(),make_ref()},
    ConnParams = connection_params(DbParams),
    Pid = spawn(fun() ->
                    ConnParams1 = case ConnParams of
                                      _ when is_map(ConnParams) -> ConnParams#{skip_monitor=>true};
                                      [Item|_]=List when is_map(Item) -> lists:map(fun(X) -> X#{skip_monitor=>true} end, List);
                                      [{_,_}|_]=List -> [{skip_monitor,true}|List];
                                      [[{_,_}|_]|_]=List -> lists:map(fun(P) -> [{skip_monitor,true}|P] end, List)
                                  end,
                    case ?Connector:connect_db(ConnParams1) of
                        {ok,Conn} -> Self ! {conn_created,Ref,Conn};
                        {error,_}=Err -> Self ! {conn_failed,Ref,Err}
                    end,
                    timer:sleep(1000) % not to finish before demonitor in pool
                end),
    MonRef = erlang:monitor(process,Pid),
    Connector = {Ref,Pid,MonRef,DbParams},
    State#poolstate{connectors=[Connector|Connectors]}.

%% ------------------------
%% @private
drop_connection(Conn,#poolstate{used_conns=UsedConns,free_conns=FreeConns,conns=Conns}=State) ->
    DbParams = maps:get(Conn,Conns,undefined),
    case lists:keytake(Conn,1,UsedConns) of
        {value,_,RestU} -> State#poolstate{used_conns=RestU,
                                           conns=maps:without([Conn],Conns)};
        false when DbParams == undefined -> State;
        false ->
            FreeList = lists:filter(fun(ConnInfo) -> maps:get(conn,ConnInfo)/=Conn end, maps:get(DbParams,FreeConns,[])),
            State#poolstate{free_conns=update_map_list(FreeConns,DbParams,FreeList),
                            conns=maps:without([Conn],Conns)}
    end.

%% ----------------------------------------------------
%% @private
used(ConnItem) -> ConnItem#{used_ts => ?BU:timestamp()}.

%% @private
free(ConnItem) -> ConnItem#{free_ts => ?BU:timestamp()}.

%% @private
new_conn(DbParams,Conn) ->
    NowTS = ?BU:timestamp(),
    #{conn => Conn,
      db_params => DbParams,
      created_ts => NowTS,
      free_ts => NowTS}.

%% @private
update_map_list(Map,Key,[]) -> maps:without([Key],Map);
update_map_list(Map,Key,List) -> Map#{Key => List}.