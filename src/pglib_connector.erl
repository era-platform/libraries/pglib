%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.05.2021
%%% @doc Common database access functions.
%%%     Opts for connector:
%%%         host, port, login, pwd, database
%%%         ignore_recovery (true)
%%%         skip_monitor (false)

-module(pglib_connector).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([connect_db/1,
         connect_db_returnargs/1,
         close_db/1,
         query_db/2,
         query_db_tout/3,
         try_query_to_db/2, try_query_to_db/3,
         ext_query_db/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(ReconnectAttemptsCount, 2).
-define(ReconnectPauseMs, 1000).

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------------------
%% attempt connect to db (one of connection props)
%% --------------------------
-spec connect_db(Params :: map() | [map()] | PropList | [PropList]) ->
                    {ok,Conn::pid()} | {error, Reason::term()} | any()
    when PropList::[{K::atom(),V::binary()|integer()}].
%% --------------------------
connect_db(L) ->
    case connect_db_1(L) of
        {ok, {C, _}} -> {ok,C};
        {error,_}=Err -> Err
    end.

%% --------------------------
%% attempt connect to db (one of connection props) and return used connection props on success
%% --------------------------
-spec connect_db_returnargs(Params :: map() | [map()] | PropList | [PropList]) ->
                                {ok,Conn::pid(),Args::map()} | {error, Reason::term()} | any()
    when PropList::[{K::atom(),V::binary()|integer()}].
%% --------------------------
connect_db_returnargs(Params) ->
    case connect_db_1(Params) of
        {ok, {C, Args}} when is_list(Args) -> {ok,C,maps:from_list(Args)};
        {error,_}=Err -> Err
    end.

%% -----
%% @private
%% -----
% simple
connect_db_1(#{}=Params) -> connect_db_2(maps:to_list(Params), reconnect_attempts_count());
connect_db_1([H|_]=Params) when is_tuple(H) -> connect_db_2(Params, reconnect_attempts_count());
% multi
connect_db_1([H|_]=Params) when is_map(H) ->
    connect_db_1(lists:map(fun(A) when is_map(A) -> maps:to_list(A); (A) -> A end, Params));
connect_db_1([H|_]=Params) when is_list(H) ->
    connect_db_2(Params, reconnect_attempts_count()).

%% @private
connect_db_2(Params, Attempts) ->
    Res = lists:foldl(fun(_, {ok,_}=Acc) -> Acc;
                         (Args, _) -> do_connect_db(Args)
                      end, {error, <<"Invalid configuration. No db connection opts found">>}, Params),
    case Res of
        {ok,_}=Ok -> Ok;
        _ when Attempts > 0 ->
            timer:sleep(reconnect_pause_ms()), % wait before new attempt
            connect_db_2(Params,Attempts-1);
        _ -> Res
    end.

%% @private
reconnect_attempts_count() -> ?CFG:reconnect_attempts_count(?ReconnectAttemptsCount).

%% @private
reconnect_pause_ms() -> ?CFG:reconnect_pause_ms(?ReconnectPauseMs).

%% -----
%% @private
%% -----
do_connect_db(Args) ->
    ?LOG('$trace', "DB Connection by ~p", [Args]),
    % parse opts
    {_, Host}=H = lists:keyfind(host, 1, Args),
    {_, Port} = lists:keyfind(port, 1, Args),
    {_, Login}=L = lists:keyfind(login, 1, Args),
    {_, Pwd0}=P = lists:keyfind(pwd, 1, Args),
    % TODO dehash pwd
    Pwd = Pwd0, % ?BU:to_list(?ENVPWD:dehash(?BU:to_binary(Pwd0))),
    % if connector should ignore recovery server
    IgnoreRecovery = lists:keyfind(ignore_recovery, 1, Args),
    % if connector should not use monitor
    SkipEnvMonitor = lists:keyfind(skip_monitor, 1, Args),
    % remain database, [port], ...
    Opts = Args--[H,L,P,IgnoreRecovery,SkipEnvMonitor], % default timeout 5000 ms
    Self = self(),
    FConnect = fun() ->
                       case gen_tcp:connect(Host, Port, [binary, {packet, 0}], 5000) of
                           {error,_}=Err ->
                               Ref0 = make_ref(),
                               Self ! Err,
                               Ref0;
                           {ok,TcpPort} ->
                               gen_tcp:close(TcpPort),
                               % connect to db
                               {ok, C} = epgsql_sock:start_link(),
                               unlink(C),
                               MonRefC = erlang:monitor(process,C),
                               Ref = epgsqli:connect(C, Host, Login, Pwd, [{timeout,5000}|Opts]),
                               F = fun F(Timeout,TimeoutMsg) ->
                                           receive
                                               {C,Ref,connected}=Res when IgnoreRecovery == {ignore_recovery,false} -> Self ! Res;
                                               {C,Ref,connected}=Res ->
                                                   case is_recovery(C, {Host,Login,Pwd,Opts}) of
                                                       false -> Self ! Res;
                                                       true -> Self ! {C,Ref,{error,is_recovery}}
                                                   end;
                                               {_,Ref, {error,_}}=Res -> Self ! Res;
                                               {'EXIT', C, _}=ExitRes -> F(2000,ExitRes);
                                               {'DOWN',MonRefC,process,C,Reason} -> F(2000,{C,Ref,{error,{process_down,Reason}}})
                                           after Timeout -> Self ! TimeoutMsg
                                           end end,
                               F(20000,{internal_error,timeout})
                       end
               end,
    {SPid,SRef} = erlang:spawn_monitor(FConnect),
    FDeMon = fun() -> erlang:demonitor(SRef, [flush]) end,
    ?LOG('$trace', "DB Connection ~ts:~p spawn pid=~120p, ref=~120p", [Host,Port,SPid,SRef]),
    % wait for results
    receive
        {C, _Ref, connected}=_X ->
            FDeMon(),
            % 05.01.2021 EnvMonitor should close_db even if role is takeovered. Link doesn't guarantee this. Opposite monitor instead.
            % % 08.11.2016 Peter
            % %  link back to general process
            % link(C),
            case SkipEnvMonitor of
                {_,true} -> ok;
                Val when Val==false orelse element(2,Val)==false ->
                    OwnerPid = self(),
                    ?LOG('$trace', "DB Connection Monitor to ~p", [OwnerPid]),
                    ?BLmonitor:append_fun(OwnerPid, C, fun(_) -> ?LOG('$warning', "Monitor. Auto closing db connection ~p owned by ~p", [C, OwnerPid]), do_close_db(C) end),
                    ?BLmonitor:append_fun(C, OwnerPid, fun(Reason) -> OwnerPid ! {'EXIT',C,{Reason}} end)
            end,
            {ok, {C, Args}};
        {C, _Ref, Error={error, _}}=_X ->
            FDeMon(),
            epgsqli:close(C),
            Error;
        %{error,Error}; % ?APP_MC had this (but why nesting {error,{error,_}} ??
        {error, _}=Error ->
            FDeMon(),
            Error;
        {'EXIT', _C, Reason}=_X ->
            ?LOG('$error', "DB Connection connector spawn exited! Reason: ~120tp", [Reason]),
            FDeMon(),
            {error, closed};
        %{error, {'EXIT',_Reason}} % ?APP_MC had this
        {internal_error,timeout}=_X ->
            ?LOG('$error', "DB Connection connector timeout!",[]),
            {error, timeout};
        {'DOWN',SRef,process,SPid,Reason}=_X ->
            ?LOG('$error', "DB Connection connector spawn down! Reason: ~120tp", [Reason]),
            {error, spawn_down}
    end.

%% @private
%% Return true if connected server is in recovery state (read-only)
is_recovery(Conn, {_Host,_Login,_Pwd,_Opts}) ->
    IsRecovery = case catch query_db(Conn,"select pg_is_in_recovery();") of
                     {ok,_Cols,[{IsRecoveryBin}]} -> ?BU:to_bool(IsRecoveryBin);
                     {ok,_,_Cols,[{IsRecoveryBin}]} -> ?BU:to_bool(IsRecoveryBin);
                     _ -> false
                 end,
    case IsRecovery of
        true -> ?LOG('$trace', "DB Connection found is recovery: ~120p (~ts)", [Conn, _Host]);
        false -> ok
    end,
    IsRecovery.

%% --------------------------
-spec close_db(Conn::pid()) -> ok.
%% --------------------------
close_db(Conn) ->
    % close connection
    Self = self(),
    case whereis(?BLmonitor) of
        Pid when is_pid(Pid) ->
            ?BLmonitor:drop_fun(Self, Conn),
            ?BLmonitor:drop_fun(Conn, Self);
        _ -> ok
    end,
    do_close_db(Conn).

%% @private
do_close_db(Conn) ->
    % close connection
    epgsqli:close(Conn),
    ok.

%% --------------------------
%% epgsql Simple Query
%% --------------------------
-spec query_db(Conn::pid(), Sql::string()) -> Result::term().
%% --------------------------
query_db(Conn, Sql) ->
    query_db_tout(Conn, Sql, infinity).

%% --------------------------
-spec query_db_tout(Conn::pid(), Sql::string(), Tout::integer()|infinity) -> Result::term().
%% --------------------------
query_db_tout(Conn, Sql, Tout) ->
    % check
    case erlang:process_info(Conn, status) of
        undefined -> throw({error, closed});
        _ ->
            % query
            Ref = epgsqli:squery(Conn, Sql),
            % wait for results
            case receive_results(Conn, Ref, [], Tout) of
                [Result] -> Result;
                Results -> Results
            end end.

%%----------------------------------------------
%% epgsql Extended Query
%%----------------------------------------------
ext_query_db(Conn, Sql, Params) ->
    % check
    case erlang:process_info(Conn, status) of
        undefined -> throw({error, closed});
        _ ->
            % query
            Ref = epgsqli:equery(Conn, Sql, Params),
            % wait for results
            case receive_results(Conn, Ref, [], infinity) of
                [Result] -> Result;
                Results -> Results
            end end.

%%---------------------------------
-spec try_query_to_db(Conn::pid(),Sql::string()) -> {ok,Res::term()} | {error,Reason::term()} | [{ok,Res::term()}|{ok,Headings::term(),Res::term()}].
%% --------------------------
try_query_to_db(Conn,Sql) ->
    try_query_to_db(Conn,Sql,infinity).

%% --------------------------
-spec try_query_to_db(Conn::pid(),Sql::string(),QueryTout::integer()|infinity) -> {ok,Res::term()} | {error,Reason::term()} | [{ok,Res::term()}|{ok,Headings::term(),Res::term()}].
%% --------------------------
try_query_to_db(Conn,Sql,QueryTout) ->
    try query_db_tout(Conn,Sql,QueryTout) of
        {ok,_}=Ok -> Ok;
        {ok,_,Res} -> {ok,Res};
        {ok,_,_,Res} -> {ok,Res};
        {error,_}=Err -> Err;
        Res when is_list(Res) ->
            case lists:all(fun(X) when is_tuple(X),element(1,X)==ok -> true;
                              (_) -> false end,Res) of
                true -> {ok,Res};
                false -> {error,Res}
            end;
        _Oth -> {error,_Oth}
    catch throw:E -> E
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---------------------------
receive_results(C, Ref, Results, Timeout) ->
    MonRef = erlang:monitor(process,C),
    Res = do_receive_results(C, MonRef, Ref, Results, Timeout),
    erlang:demonitor(MonRef),
    Res.

do_receive_results(C, MonRef, Ref, Results, Timeout) ->
    try do_receive_result(C, MonRef, Ref, [], [],Timeout) of
        done -> lists:reverse(Results);
        R -> do_receive_results(C, MonRef, Ref, [R|Results], Timeout)
    catch throw:E -> E
    end.

%% ---------------------------
do_receive_result(C, MonRef, Ref, Cols, Rows, Timeout) ->
    receive
        {C, Ref, {columns, Cols2}} ->
            do_receive_result(C, MonRef, Ref, Cols2, Rows, Timeout);
        {C, Ref, {data, Row}} ->
            do_receive_result(C, MonRef, Ref, Cols, [Row|Rows], Timeout);
        {C, Ref, {error, _E} = Error} ->
            Error;
        {C, Ref, {complete, {_Type, Count}}} ->
            case Rows of
                [] -> {ok, Count};
                _L -> {ok, Count, Cols, lists:reverse(Rows)}
            end;
        {C, Ref, {complete, _Type}} ->
            {ok, Cols, lists:reverse(Rows)};
        {C, Ref, done} ->
            done;
        {'DOWN', MonRef, process, C, _Reason} ->
            ?LOG('$error',"Pg connection pid found down ~tp (~120tp)",[C,_Reason]),
            throw({error, connection_pid_down});
        {'EXIT', C, _Reason} ->
            ?LOG('$error',"Pg connection pid found exited ~tp (~120tp)",[C,_Reason]),
            throw({error, closed})
    after Timeout ->
            ?LOG('$error',"Pg connection pid wait timeout: ~tp",[C]),
            ok = epgsql:cancel(C),
            throw({error,timeout})
    end.
